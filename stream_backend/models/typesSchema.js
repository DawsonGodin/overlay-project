const { Schema, model } = require('mongoose');

const typesSchema = new Schema({
  userId: {
    type: String,
    required: true
  },
  nameId: {
    type: String,
    required: true
  },
  commandPosId: {
    type: String,
    required: true
  },
  commandDeptId: {
    type: String,
    required: true
  },
  position: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  department: {
    type: String,
    required: true
  }
});

module.exports = model('typesSchema', typesSchema, 'typesSchema');