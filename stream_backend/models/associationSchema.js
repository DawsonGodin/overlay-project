const { Schema, model } = require('mongoose');

const associationSchema = new Schema({
  userId: {
    type: String,
    required: true
  },
  discordServerId: {
    type: String,
    required: true
  },
  channelName: {
    type: String,
    required: true
  },
  authorizedDiscordRole: {
    type: String,
    required: true
  },
  requiredDiscordChannel: {
    type: String,
    required: false
  },
  lastStreamTitle: {
    type: String,
    required: false
  },
  lastStreamStart: {
    type: String,
    required: false
  }
});

module.exports = model('associationSchema', associationSchema, 'associationSchema');