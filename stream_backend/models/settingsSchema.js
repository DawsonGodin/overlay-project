const { Schema, model } = require('mongoose');

const settingsSchema = new Schema({
  userId: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true
  }
});

module.exports = model('settingsSchema', settingsSchema, 'settingsSchema');