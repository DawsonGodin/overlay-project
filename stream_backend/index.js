const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const tmi = require("tmi.js");
const logger = require("morgan");
const axios = require("axios").default;
const Discord = require("discord.js");
const Mongoose = require("mongoose");
const moment = require("moment-timezone");
const bot = new Discord.Client();

bot.login();

// Mongoose Stuff
const settingsSchema = require("./models/settingsSchema");
const associationSchema = require("./models/associationSchema");
const typesSchema = require("./models/typesSchema");

Mongoose.connect("mongodb://localhost:27017/StreamDB", {
  authSource: "admin",
  user: "DawsonG",
  pass: "Frootluips2000!",
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
Mongoose.connection.on("error", (err) => {
  throw err;
});
Mongoose.connection.once("open", async () => {
  console.log("Connected to MongoDB!");
});

let tts = false;
let streams = [];
let bearerToken = "";
let seatbeltTimeout = false;
let messageCount = 0;

let statuses = [
  "10-5",
  "10-6",
  "10-7",
  "10-8",
  "10-8-11",
  "10-15",
  "10-23",
  "10-97",
  "10-99",
];
let countdownOptions = ["start", "end"];
let servers = ["1", "2"];
let randoMessages = [
  "Attention MidwestRP members in my stream... If you're in the same server as I am (ref: overlay) then I will kindly ask for you to get out of my stream please, I don't want to get you in trouble for violating the R&R, ily!",
  "Make sure you join my Discord, it's kewl! https://links.dawsoncan.dev/discord",
  "Hey you! Yeah you! Make sure to follow me on Instagram: https://links.dawsoncan.dev/instagram",
  "Bro! I don't think you're following me on Twitter yet, do it now: https://links.dawsoncan.dev/twitter",
  "Subscribe to my YouTube, I might upload there from time to time... https://links.dawsoncan.dev/youtube",
];

let types = [
  {
    id: "BCSO_ACE",
    commandId: "ace",
    position: "Senior ACE Officer",
    name: "Dawson Godin [7E-15]",
    department: "Blaine County Sheriff's Office",
    departmentShort: "bcso",
  },
  {
    id: "BCSO_ACE2",
    commandId: "ace2",
    position: "Senior ACE Officer",
    name: "Dawson Godin [7E-15]",
    department: "Blaine County Sheriff's Office",
    departmentShort: "bcso",
  },
  {
    id: "BCSO_MBU",
    commandId: "mbu",
    position: "Senior MBU Officer",
    name: "Dawson Godin [7E-15]",
    department: "Blaine County Sheriff's Office",
    departmentShort: "bcso",
  },
  {
    id: "BCSO_TSD",
    commandId: "tsd",
    position: "Senior TSD Officer",
    name: "Dawson Godin [7E-15]",
    department: "Blaine County Sheriff's Office",
    departmentShort: "bcso",
  },
  {
    id: "BCSO_CID",
    commandId: "ci",
    position: "Senior CI Detective",
    name: "Dawson Godin [7E-15]",
    department: "Blaine County Sheriff's Office",
    departmentShort: "bcso",
  },
  {
    id: "BCSO_Patrol",
    commandId: "patrol",
    position: "Senior Deputy",
    name: "Dawson Godin [7E-15]",
    department: "Blaine County Sheriff's Office",
    departmentShort: "bcso",
  },
  {
    id: "LSPD_Patrol",
    commandId: "patrol",
    position: "Senior Officer",
    name: "Dawson Godin [7E-15]",
    department: "Los Santos Police Department",
    departmentShort: "lspd",
  },
  {
    id: "LSPD_K9",
    commandId: "k9hp",
    position: "SAHP K9 Handler",
    name: "Dawson Godin [7E-15]",
    department: "Los Santos Police Department",
    departmentShort: "lspd",
  },
  {
    id: "LSPD_K9",
    commandId: "k9so",
    position: "BCSO K9 Handler",
    name: "Dawson Godin [7E-15]",
    department: "Los Santos Police Department",
    departmentShort: "lspd",
  },
  {
    id: "LSPD_K9",
    commandId: "k9pd",
    position: "LSPD K9 Handler",
    name: "Dawson Godin [7E-15]",
    department: "Los Santos Police Department",
    departmentShort: "lspd",
  },
  {
    id: "SAHP_Patrol",
    commandId: "patrol",
    position: "Master Trooper",
    name: "Dawson Godin [7E-15]",
    department: "San Andreas Highway Patrol",
    departmentShort: "sahp",
  },
];

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger("dev"));

app.get("/", (req, res) => {
  res.status(200).send("Reached!");
});

app.post("/status/:code?", (req, res) => {
  let newCode = req.query.code;
  status = newCode;
  if (newCode === "10-8") {
    call = false;
  }
  io.emit("emit data", {
    status,
    call,
    currentType,
    position,
    name,
    department,
    currentServer,
  });
  res.status(200).send("Updated!");
});

app.post("/call", (req, res) => {
  const { details } = req.body;

  if (!details) return res.status(400).send("No details given!");

  call = details;
  if (details !== false) {
    status = "10-97";
  }
  io.emit("emit data", {
    status,
    call,
    currentType,
    position,
    name,
    department,
    currentServer,
  });
  res.status(200).send("Updated!");
});

app.post("/active", (req, res) => {
  const { type } = req.body;

  const foundType = types.filter((typ) => {
    return typ.id === type;
  });

  if (foundType.length < 1) return res.status(400).send("Invalid type given!");

  types.forEach((typeItem) => {
    if (typeItem.id === type) {
      currentType = type;
      position = typeItem.position;
      name = typeItem.name;
      department = typeItem.department;
      io.emit("emit data", {
        status,
        call,
        currentType,
        position,
        name,
        department,
        currentServer,
      });
      res.status(200).send("Updated!");
    }
  });
});

app.post("/twitch/streams", async (req, res) => {
  if (req.body.challenge) return res.status(200).send(req.body.challenge);
  console.log(req);
  const { subscription, event } = req.body;
  const twitchModChannel = await bot.channels.cache.get("778525504948797440");
  const streamNotifChannel = await bot.channels.cache.get("779148645919752222");
  res.status(200).send("Thank you for the information!");
  switch (subscription.type) {
    case "channel.update":
      const twitchChannelUpdateEmbed = new Discord.MessageEmbed()
        .setColor("ffac33")
        .setTitle("Twitch Channel Updated")
        .setAuthor(event.broadcaster_user_name)
        .setURL(`https://twitch.tv/${event.broadcaster_user_name}`)
        .setDescription(
          `**Title:** \`${event.title}\`\n**Category:** \`${
            event.category_name
          }\`\n**Language:** \`${event.language}\`\n**Mature:** ${
            event.is_mature ? "<a:checky:720377734978601000>" : ":x:"
          }`
        )
        .setTimestamp();
      twitchModChannel.send(twitchChannelUpdateEmbed);

      if (event.broadcaster_user_name.toLowerCase() === "dawsongodin") {
        await associationSchema.findOneAndUpdate(
          { channelName: event.broadcaster_user_name.toLowerCase() },
          { lastStreamTitle: event.title }
        );
      }
      break;
    case "stream.online":
      setTimeout(() => {
        axios
          .get(
            `https://api.twitch.tv/helix/streams?user_id=${event.broadcaster_user_id}`,
            {
              headers: {
                Authorization: `Bearer ${bearerToken}`,
                "Client-Id": "",
              },
            }
          )
          .then((res) => {
            const parsedStreamData = res.data.data[0];
            axios
              .get(
                `https://api.twitch.tv/helix/users?id=${event.broadcaster_user_id}`,
                {
                  headers: {
                    Authorization: `Bearer ${bearerToken}`,
                    "Client-Id": "",
                  },
                }
              )
              .then((resp) => {
                const parsedUserData = resp.data.data[0];
                axios
                  .get(
                    `https://api.twitch.tv/helix/users/follows?to_id=${event.broadcaster_user_id}`,
                    {
                      headers: {
                        Authorization: `Bearer ${bearerToken}`,
                        "Client-Id": "",
                      },
                    }
                  )
                  .then((respo) => {
                    const totalFollowers = respo.data.total;
                    const streamNotificationEmbed = new Discord.MessageEmbed()
                      .setColor("ffac33")
                      .setAuthor(parsedStreamData.user_name)
                      .setImage(
                        `https://static-cdn.jtvnw.net/previews-ttv/live_user_${parsedStreamData.user_name.toLowerCase()}-1920x1080.jpg`
                      )
                      .setThumbnail(parsedUserData.profile_image_url)
                      .setTitle(parsedStreamData.title)
                      .setURL(`https://twitch.tv/${parsedStreamData.user_name}`)
                      .setFooter(`Playing: ${parsedStreamData.game_name}`)
                      // .addField('Current Viewers', parsedStreamData.viewer_count, true)
                      .addField("Followers", totalFollowers, true)
                      .addField("Total Views", parsedUserData.view_count, true)
                      .setTimestamp();
                    streamNotifChannel
                      .send(
                        `${
                          parsedStreamData.user_name
                        } is now <:LIVE:783073054309482506>! ${
                          parsedStreamData.user_name === "DawsonGodin"
                            ? "<@&783073440257146943> *Bop! Over N' Out!*"
                            : ""
                        }`,
                        {
                          embed: streamNotificationEmbed,
                        }
                      )
                      .then((m) => {
                        streams.push({
                          streamer: event.broadcaster_user_name,
                          message_id: m.id,
                        });
                      });
                    if (parsedStreamData.user_name === "DawsonGodin") {
                      bot.user.setActivity("DawsonGodin on Twitch", {
                        type: "STREAMING",
                        url: "https://twitch.tv/dawsongodin",
                      });
                      associationSchema.findOneAndUpdate(
                        { channelName: "dawsongodin" },
                        { lastStreamStart: moment().tz("America/Los_Angeles") }
                      );
                    }
                  })
                  .catch((error) => {
                    console.error(error);
                  });
              })
              .catch((erro) => {
                console.error(erro);
              });
          })
          .catch((err) => {
            console.error(err);
          });
      }, 30000);
      break;
    case "stream.offline":
      streams.forEach((stre) => {
        if (stre.streamer === event.broadcaster_user_id) {
          streamNotifChannel.messages
            .fetch(stre.message_id)
            .then((m) => m.delete());
        }
      });
      if (event.broadcaster_user_name === "DawsonGodin") {
        bot.user.setActivity("Waffle House Simulator", { type: "PLAYING" });
      }
      break;
    case "channel.follow":
      tmiClient.say(
        `#${event.broadcaster_user_name}`,
        `Thank you @${event.user_name} for following @DawsonGodin! Here's some waffles dawson22Waffles`
      );
      break;
  }
});

io.on("connection", (socket) => {
  console.log("A new user connected!");
  // console.log('Socket:', socket);
  // io.emit('emit data', {status, call, currentType, position, name, department, currentServer});

  socket.on("requestData", async (id, callback) => {
    const result = await settingsSchema.find({ userId: id }).exec();
    if (result.length === 0) return callback("Nope!");
    let dataArray = {};
    result.forEach((reso) => {
      dataArray[`${reso.type}`] = reso.value;
    });
    callback(dataArray);
  });

  socket.on("voiceRecognition_dispatch", (type, data, fn) => {
    switch (type) {
      case "statusChange":
        switch (data) {
          case "10-8":
            status = "10-8";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit("voiceSynthesis_dispatch", "statusChange", "10-8");
            break;
          case "10-7":
            status = "10-7";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit("voiceSynthesis_dispatch", "statusChange", "10-7");
            break;
          case "10-6":
            status = "10-6";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit("voiceSynthesis_dispatch", "statusChange", "10-6");
            break;
          case "10-5":
            status = "10-5";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit("voiceSynthesis_dispatch", "statusChange", "10-5");
            break;
          case "10-97":
            status = "10-97";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit(
                "voiceSynthesis_dispatch",
                "statusChange",
                "10-97"
              );
            break;
          case "10-23":
            status = "10-23";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit(
                "voiceSynthesis_dispatch",
                "statusChange",
                "10-97"
              );
            break;
          case "10-8-11":
            status = "10-8-11";
            io.emit("emit data", {
              status,
              call,
              currentType,
              position,
              name,
              department,
              currentServer,
            });
            if (tts)
              return io.emit(
                "voiceSynthesis_dispatch",
                "statusChange",
                "10-8-11"
              );
            break;
        }
        break;
    }
    fn("Recieved voiceRecognition_dispatch");
  });
});

http.listen(5006, () => {
  console.log("Listening on port: 5006");
});

// Discord Bot

bot.on("ready", () => {
  bot.user
    .setActivity("Waffle House Simulator", {
      type: "PLAYING",
      url: "https://twitch.tv/dawsongodin",
    })
    .then(console.log(`${bot.user.tag} is now online!`));
});

bot.on("message", async (message) => {
  if (message.author.bot) return;
  if (!message.guild) return;
  if (!message.content.startsWith("!")) return;
  let args = message.content.slice(1).split(" ");
  command = args.shift();

  const commands = [
    "countdown",
    "mwnotice",
    "10-11",
    "status",
    "activate",
    "server",
    "call",
    "callos",
    "discord",
    "mwrp",
    "midwest",
    "midwestrp",
    "instagram",
    "twitter",
    "youtube",
    "overlay",
    "lksdjf",
  ];

  if (!commands.includes(command.toLowerCase())) return;

  message.delete();

  const {
    _id,
    userId,
    discordServerId,
    channelName,
    authorizedDiscordRole,
    requiredDiscordChannel,
    lastStreamStart,
    lastStreamTitle,
  } = await associationSchema.findOne({ discordServerId: message.guild.id });
  if (_id === null) return;
  if (requiredDiscordChannel) {
    if (message.channel.id !== requiredDiscordChannel)
      return message
        .reply(`this must be sent in <#${requiredDiscordChannel}>!`)
        .then((m) => m.delete({ timeout: 3000 }));
  }
  if (
    !message.member.roles.cache.some(
      (role) => role.id === authorizedDiscordRole
    )
  )
    return message
      .reply("you must have the authorized role to be able to do this command!")
      .then((m) => m.delete({ timeout: 3000 }));
  const { milliseconds, seconds, minutes, hours, days, months, years } =
    moment.duration(new Date() - lastStreamStart)._data;

  const callTimeStampChannel = bot.channels.cache.get("789668059270610964");

  if (command.toLowerCase() === "countdown") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (!args[0])
      return message
        .reply("you didn't specify whether to start or end the countdown!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (!countdownOptions.some((opt) => opt === args[0]))
      return message
        .reply("invalid countdown option given!")
        .then((m) => m.delete({ timeout: 3000 }));
    switch (args[0]) {
      case "start":
        countdown = true;
        break;

      case "end":
        countdown = false;
        break;
    }
    io.emit("countdown", { countdownBool: countdown });
    console.log(`Sent countdown information: ${countdown}!`);
  }

  if (command.toLowerCase() === "mwnotice") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "Attention MidwestRP members in my stream... If you're in the same server as I am (ref: overlay) then I will kindly ask for you to get out of my stream please, I don't want to get you in trouble for violating the R&R, ily!"
    );
  }

  if (command.toLowerCase() === "discord") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "Check out DawsonGodin's Discord: https://links.dawsoncan.dev/discord"
    );
  }

  if (command.toLowerCase() === "donate") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "If you want to donate which you don't have to at all here's the link: https://streamelements.com/dawsongodin/tip"
    );
  }

  if (
    command.toLowerCase() === "midwest" ||
    command.toLowerCase() === "mwrp" ||
    command.toLowerCase() === "midwestrp"
  ) {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "MidwestRP is a FiveM whitelisted community that has grown to over 900+ members. Interested? Check out https://community.midwestrp.net/"
    );
  }

  if (command.toLowerCase() === "twitter") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "Check out DawsonGodin's Twitter: https://links.dawsoncan.dev/twitter"
    );
  }

  if (command.toLowerCase() === "instagram") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "Check out DawsonGodin's Instagram: https://links.dawsoncan.dev/instagram"
    );
  }

  if (command.toLowerCase() === "youtube") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "Check out DawsonGodin's YouTube: https://links.dawsoncan.dev/youtube"
    );
  }

  if (command.toLowerCase() === "overlay") {
    if (
      !message.member.roles.cache.some(
        (role) => role.id === "786348315369406534"
      )
    )
      return message
        .reply("you must be a Twitch Moderator to do this!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (message.channel.id !== "782528322256371732")
      return message
        .reply("this must be sent in <#782528322256371732>!")
        .then((m) => m.delete({ timeout: 3000 }));
    tmiClient.say(
      "#dawsongodin",
      "DawsonGodin's overlay is custom made by himself, it allows for any of the mods to change my status, department, or even his call information through Discord or Twitch. There's voice recognition for DawsonGodin which works most of the time since it is still a work in progress."
    );
  }

  if (command.toLowerCase() === "10-11") {
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: "10-23" }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "call" },
      { value: "10-11 // Traffic Stop" }
    );
    io.emit("emit data", {
      userId,
      status: "10-23",
      call: "10-11 // Traffic Stop",
    });
    message.reply(
      `successfully updated **DawsonGodin**'s call details to: \`10-11 // Traffic Stop\` & updated **DawsonGodin**'s status to: \`10-23\`!`
    );
    callTimeStampChannel.send(
      `**Stream Title:** ${lastStreamTitle}\n**Date of Stream:** ${moment()
        .tz("America/Los_Angeles")
        .format(
          "M/D/YYYY"
        )}\n**Start of Call Timestamp:** ${hours}:${minutes}:${seconds}\n**Brief Description of Call:** 10-11 Traffic Stop`
    );
  }

  if (command.toLowerCase() === "status") {
    if (!args[0])
      return message
        .reply("you didn't specify a status to update with!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (!statuses.some((status) => status === args[0]))
      return message
        .reply("invalid status given!")
        .then((m) => m.delete({ timeout: 3000 }));
    await settingsSchema.findOneAndUpdate(
      { userId: userId, type: "status" },
      { value: args[0] }
    );
    io.emit("emit data", { userId: userId, status: args[0] });
    if (args[0] === "10-8") {
      await settingsSchema.findOneAndUpdate(
        { userId: userId, type: "call" },
        { value: "false" }
      );
      io.emit("emit data", { userId, call: "false" });
    }
    message.reply(
      `successfully updated **DawsonGodin**'s status to: \`${args[0]}\`!`
    );
  }

  if (command.toLowerCase() === "server") {
    if (!args[0])
      return message
        .reply("you didn't specify a server to change to!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (!servers.some((server) => server === args[0]))
      return message
        .reply("invalid status given!")
        .then((m) => m.delete({ timeout: 3000 }));
    await settingsSchema.findOneAndUpdate(
      { userId, type: "currentServer" },
      { value: args[0] }
    );
    io.emit("emit data", { userId, currentServer: args[0] });
    message.reply(
      `successfully updated **DawsonGodin**'s server to: \`${args[0]}\`!`
    );
  }

  if (command.toLowerCase() === "activate") {
    const currentTypes = await typesSchema.find({ userId }).exec();
    if (currentTypes.length === 0)
      return message.reply("no activation positions have been setup yet!");
    if (!args[0])
      return message
        .reply("you didn't specify a department to activate with!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (!args[1])
      return message
        .reply("you didn't specify a position to activate with!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (
      !currentTypes.some(
        (types) =>
          types.commandDeptId === args[0].toLowerCase() &&
          types.commandPosId === args[1].toLowerCase()
      )
    )
      return message
        .reply("invalid department & position given!")
        .then((m) => m.delete({ timeout: 3000 }));
    const chosenType = await typesSchema
      .findOne({ userId, commandDeptId: args[0], commandPosId: args[1] })
      .exec();
    if (chosenType._id === null)
      return message.reply(
        "could not find department & position you supplied, contact Dawson#0001 for help!"
      );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "currentType" },
      { value: chosenType.nameId }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "position" },
      { value: chosenType.position }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "name" },
      { value: chosenType.name }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "department" },
      { value: chosenType.department }
    );
    io.emit("emit data", {
      userId,
      currentType: chosenType.nameId,
      position: chosenType.position,
      name: chosenType.name,
      department: chosenType.department,
    });
    message.reply(
      `successfully updated **DawsonGodin**'s activation to...\nPosition: \`${chosenType.position}\`\nName: \`${chosenType.name}\`\nDepartment: \`${chosenType.department}\``
    );
  }

  if (command.toLowerCase() === "clear" || command.toLowerCase() === "cancel") {
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: "10-8" }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "call" },
      { value: "false" }
    );
    io.emit("emit data", { userId, status: "10-8", call: "false" });
  }

  if (command.toLowerCase() === "call") {
    if (!args[0])
      return message
        .reply("you didn't specify any call details!")
        .then((m) => m.delete({ timeout: 3000 }));
    if (
      args[0].toLowerCase() === "cancel" ||
      args[0].toLowerCase() === "clear"
    ) {
      await settingsSchema.findOneAndUpdate(
        { userId, type: "status" },
        { value: "10-8" }
      );
      await settingsSchema.findOneAndUpdate(
        { userId, type: "call" },
        { value: "false" }
      );
      io.emit("emit data", { userId, status: "10-8", call: "false" });
    } else {
      await settingsSchema.findOneAndUpdate(
        { userId, type: "status" },
        { value: "10-97" }
      );
      await settingsSchema.findOneAndUpdate(
        { userId, type: "call" },
        { value: args.join(" ") }
      );
      io.emit("emit data", { userId, status: "10-97", call: args.join(" ") });
      message.reply(
        `successfully updated **DawsonGodin**'s call to: \`${args.join(
          " "
        )}\` & updated **DawsonGodin**'s status to: \`10-97\`!`
      );

      callTimeStampChannel.send(
        `**Stream Title:** ${lastStreamTitle}\n**Date of Stream:** ${moment()
          .tz("America/Los_Angeles")
          .format(
            "M/D/YYYY"
          )}\n**Start of Call Timestamp:** ${hours}:${minutes}:${seconds}\n**Brief Description of Call:** ${args.join(
          " "
        )}`
      );
    }
  }

  if (command.toLowerCase() === "callos") {
    if (!args[0])
      return message
        .reply("you didn't specify any call details!")
        .then((m) => m.delete({ timeout: 3000 }));
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: "10-23" }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "call" },
      { value: args.join(" ") }
    );
    io.emit("emit data", { userId, status: "10-23", call: args.join(" ") });
    message.reply(
      `successfully updated **DawsonGodin**'s call to: \`${args.join(
        " "
      )}\` & updated **DawsonGodin**'s status to: \`10-23\`!`
    );

    callTimeStampChannel.send(
      `**Stream Title:** ${lastStreamTitle}\n**Date of Stream:** ${moment()
        .tz("America/Los_Angeles")
        .format(
          "M/D/YYYY"
        )}\n**Start of Call Timestamp:** ${hours}:${minutes}:${seconds}\n**Brief Description of Call:** ${args.join(
        " "
      )}`
    );
  }
});

// Twitch Bot
const tmiClient = new tmi.Client({
  options: { debug: true },
  connection: {
    secure: true,
    reconnect: true,
  },
  identity: {
    username: "WafflesNSyrup",
    password: "oauth:",
  },
});

tmiClient.connect().then(async () => {
  const channels = await associationSchema.find().exec();
  channels.forEach((chan) => {
    tmiClient.join(chan.channelName);
  });
});

tmiClient.on("message", messageHandler);

async function messageHandler(channel, tags, message, self) {
  if (self || tags["message-type"] === "whisper") return;

  if (message) {
    messageCount++;
    if (messageCount >= 40) {
      tmiClient.say(
        channel,
        randoMessages[Math.floor(Math.random() * randoMessages.length)]
      );
      messageCount = 0;
    }
  }

  if (message.startsWith("https://clips.twitch.tv/")) {
    const clipsChannel = await bot.channels.cache.get("783132014978269214");
    clipsChannel.send(message);
  }

  if (message.startsWith("!")) {
    const args = message.slice(1).split(" ");
    const commandName = args.shift().toLowerCase();
    const context = { channel, tags, commandName, args };
    const channelResults = await associationSchema.find();
    if (!channelResults.some((chan) => chan.channelName === channel.slice(1)))
      return;
    commandHandler({ ...context });
  }
}

async function commandHandler(context) {
  const { channel, tags, commandName, args } = context;

  const twitchModChannel = await bot.channels.cache.get("782528322256371732");

  const {
    _id,
    userId,
    discordServerId,
    channelName,
    authorizedDiscordRole,
    requiredDiscordChannel,
    lastStreamStart,
    lastStreamTitle,
  } = await associationSchema.findOne({ channelName: channel.slice(1) });
  if (!_id) return;

  const commandLogging = bot.channels.cache.get(requiredDiscordChannel);
  const { milliseconds, seconds, minutes, hours, days, months, years } =
    moment.duration(new Date() - lastStreamStart)._data;
  const callTimeStampChannel = bot.channels.cache.get("789668059270610964");

  if (
    commandName.toLowerCase() === "midwest" ||
    commandName === "midwestrp" ||
    commandName === "mwrp"
  ) {
    tmiClient.say(
      channel,
      "MidwestRP is a FiveM whitelisted community that has grown to over 900+ members. Interested? Check out https://community.midwestrp.net/"
    );
  }

  if (commandName.toLowerCase() === "mwnotice") {
    tmiClient.say(
      channel,
      "Attention MidwestRP members in my stream... If you're in the same server as I am (ref: overlay) then I will kindly ask for you to get out of my stream please, I don't want to get you in trouble for violating the R&R, ily!"
    );
  }

  if (commandName.toLowerCase() === "donate") {
    tmiClient.say(
      "#dawsongodin",
      "If you want to donate which you don't have to at all here's the link: https://streamelements.com/dawsongodin/tip"
    );
  }

  if (commandName.toLowerCase() === "twitter") {
    tmiClient.say(
      channel,
      "Check out DawsonGodin's Twitter: https://links.dawsoncan.dev/twitter"
    );
  }

  if (commandName.toLowerCase() === "instagram") {
    tmiClient.say(
      channel,
      "Check out DawsonGodin's Instagram: https://links.dawsoncan.dev/instagram"
    );
  }

  if (commandName.toLowerCase() === "discord") {
    tmiClient.say(
      channel,
      "Check out DawsonGodin's Discord: https://links.dawsoncan.dev/discord"
    );
  }

  if (commandName.toLowerCase() === "youtube") {
    tmiClient.say(
      channel,
      "Check out DawsonGodin's YouTube: https://links.dawsoncan.dev/youtube"
    );
  }

  if (commandName.toLowerCase() === "lurk") {
    tmiClient.say(channel, `@${tags.username}, thanks for the lurk!`);
  }

  if (commandName.toLowerCase() === "overlay") {
    tmiClient.say(
      channel,
      `@${tags.username}, DawsonGodin's overlay is custom made by himself, it allows for any of the mods to change my status, department, or even his call information through Discord or Twitch. There's voice recognition for DawsonGodin which works most of the time since it is still a work in progress.`
    );
  }

  if (
    commandName.toLowerCase() === "sonoran" ||
    commandName.toLowerCase() === "sonorancad"
  ) {
    tmiClient.say(
      channel,
      `@${tags.username}, Sonoran Software Systems is a Software Development company. Offering a state of the art Computed Aided Dispatch systems for gaming communities, learn more @ https://sonorancad.com!`
    );
  }

  async function checkPermissions(tags, channel) {
    if (tags.mod || tags["display-name"].toLowerCase() === channel.slice(1)) {
      return true;
    } else {
      return false;
    }
  }

  if (commandName.toLowerCase() === "activate") {
    if (!checkPermissions(tags, channel))
      return tmiClient.say(
        channel,
        `@${tags.username}, insufficient permissions!`
      );
    const currentTypes = await typesSchema.find({ userId }).exec();
    if (currentTypes.length === 0)
      return tmiClient.say(
        channel,
        `@${tags.username}, no activation positions have been setup yet!`
      );
    if (!args[0])
      return tmiClient.say(
        channel,
        `@${tags.username}, you didn\'t specify a department to activate with!`
      );
    if (!args[1])
      return tmiClient.say(
        channel,
        `@${tags.username}, you didn\'t specify a position to activate with!`
      );
    if (
      !currentTypes.some(
        (types) =>
          types.commandDeptId === args[0].toLowerCase() &&
          types.commandPosId === args[1].toLowerCase()
      )
    )
      return tmiClient.say(
        channel,
        `@${tags.username}, invalid department & position given!`
      );
    const chosenType = await typesSchema
      .findOne({ userId, commandDeptId: args[0], commandPosId: args[1] })
      .exec();
    if (chosenType._id === null)
      return tmiClient.say(
        channel,
        `@${tags.username}, could not find department & position you supplied, contact Dawson#0001 for help!`
      );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "currentType" },
      { value: chosenType.nameId }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "position" },
      { value: chosenType.position }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "name" },
      { value: chosenType.name }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "department" },
      { value: chosenType.department }
    );
    io.emit("emit data", {
      userId,
      currentType: chosenType.nameId,
      position: chosenType.position,
      name: chosenType.name,
      department: chosenType.department,
    });
    if (!commandLogging) return;
    commandLogging.send(
      `**${tags.username}** via Discord, has updated **DawsonGodin**'s activation to...\nPosition: \`${chosenType.position}\`\nName: \`${chosenType.name}\`\nDepartment: \`${chosenType.department}\``
    );
  }

  if (commandName.toLowerCase() === "10-11") {
    if (!checkPermissions(tags, channel))
      return tmiClient.say(
        channel,
        `@${tags.username}, insufficient permissions!`
      );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: "10-23" }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "call" },
      { value: "10-11 // Traffic Stop" }
    );
    io.emit("emit data", {
      userId,
      status: "10-23",
      call: "10-11 // Traffic Stop",
    });
    if (!commandLogging) return;
    commandLogging.send(
      `**${tags.username}** via Twitch, has updated **DawsonGodin**'s call details to: \`10-11 // Traffic Stop\` & updated **DawsonGodin**'s status to: \`10-23\`!`
    );

    callTimeStampChannel.send(
      `**Stream Title:** ${lastStreamTitle}\n**Date of Stream:** ${moment()
        .tz("America/Los_Angeles")
        .format(
          "M/D/YYYY"
        )}\n**Start of Call Timestamp:** ${hours}:${minutes}:${seconds}\n**Brief Description of Call:** 10-11 // Traffic Stop`
    );
  }

  if (commandName.toLowerCase() === "status") {
    if (!checkPermissions(tags, channel))
      return tmiClient.say(
        channel,
        `@${tags.username}, insufficient permissions!`
      );
    if (!args[0])
      return tmiClient.say(
        channel,
        `@${tags.username}, you didn\'t specify a status to update with!`
      );
    if (!statuses.some((status) => status === args[0]))
      return tmiClient.say(channel, `@${tags.username}, invalid status given!`);
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: args[0] }
    );
    io.emit("emit data", { userId, status: args[0] });
    if (args[0] === "10-8") {
      await settingsSchema.findOneAndUpdate(
        { userId, type: "call" },
        { value: "false" }
      );
      io.emit("emit data", { userId, call: "false" });
    }
    if (!commandLogging) return;
    commandLogging.send(
      `**${tags.username}** via Twitch, has updated **DawsonGodin**'s status to: \`${args[0]}\`!`
    );
  }

  if (commandName.toLowerCase() === "server") {
    if (!checkPermissions(tags, channel))
      return tmiClient.say(
        channel,
        `@${tags.username}, insufficient permissions!`
      );
    if (!args[0])
      return tmiClient.say(
        channel,
        `@${tags.username}, you didn\'t specify a server to change to!`
      );
    if (!servers.some((server) => server === args[0]))
      return tmiClient.say(channel, `@${tags.username}, invalid status given!`);
    await settingsSchema.findOneAndUpdate(
      { userId, type: "currentServer" },
      { value: args[0] }
    );
    io.emit("emit data", { userId, currentServer: args[0] });
    if (!commandLogging) return;
    commandLogging.send(
      `**${tags.username}** via Twitch, has updated **DawsonGodin**'s server to: \`${args[0]}\`!`
    );
  }

  if (
    commandName.toLowerCase() === "cancel" ||
    commandName.toLowerCase() === "clear"
  ) {
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: "10-8" }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "call" },
      { value: "false" }
    );
    io.emit("emit data", { userId, status: "10-8", call: "false" });
    if (!commandLogging) return;
    commandLogging.send(
      `**${tags.username}** via Twitch, has cleared **DawsonGodin**'s call!`
    );
  }

  if (commandName.toLowerCase() === "call") {
    if (!checkPermissions(tags, channel))
      return tmiClient.say(
        channel,
        `@${tags.username}, insufficient permissions!`
      );
    if (!args[0])
      return tmiClient.say(
        channel,
        `@${tags.username}, you didn\'t specify any call details!`
      );
    if (
      args[0].toLowerCase() === "cancel" ||
      args[0].toLowerCase() === "clear"
    ) {
      await settingsSchema.findOneAndUpdate(
        { userId, type: "status" },
        { value: "10-8" }
      );
      await settingsSchema.findOneAndUpdate(
        { userId, type: "call" },
        { value: "false" }
      );
      io.emit("emit data", { userId, status: "10-8", call: "false" });
      if (!commandLogging) return;
      commandLogging.send(
        `**${tags.username}** via Twitch, has cleared **DawsonGodin**'s call!`
      );
    } else {
      await settingsSchema.findOneAndUpdate(
        { userId, type: "status" },
        { value: "10-97" }
      );
      await settingsSchema.findOneAndUpdate(
        { userId, type: "call" },
        { value: args.join(" ") }
      );
      io.emit("emit data", { userId, status: "10-97", call: args.join(" ") });
      if (!commandLogging) return;
      commandLogging.send(
        `**${
          tags.username
        }** via Twitch, has updated **DawsonGodin**'s call details to: \`${args.join(
          " "
        )}\` & updated **DawsonGodin**'s status to: \`10-97\`!`
      );

      callTimeStampChannel.send(
        `**Stream Title:** ${lastStreamTitle}\n**Date of Stream:** ${moment()
          .tz("America/Los_Angeles")
          .format(
            "M/D/YYYY"
          )}\n**Start of Call Timestamp:** ${hours}:${minutes}:${seconds}\n**Brief Description of Call:** ${args.join(
          " "
        )}`
      );
    }
  }

  if (commandName.toLowerCase() === "callos") {
    if (!checkPermissions(tags, channel))
      return tmiClient.say(
        channel,
        `@${tags.username}, insufficient permissions!`
      );
    if (!args[0])
      return tmiClient.say(
        channel,
        `@${tags.username}, you didn\'t specify any call details!`
      );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "status" },
      { value: "10-23" }
    );
    await settingsSchema.findOneAndUpdate(
      { userId, type: "call" },
      { value: args.join(" ") }
    );
    io.emit("emit data", { userId, status: "10-23", call: args.join(" ") });
    if (!commandLogging) return;
    commandLogging.send(
      `**${
        tags.username
      }** via Twitch, has updated **DawsonGodin**'s call details to: \`${args.join(
        " "
      )}\` & updated **DawsonGodin**'s status to: \`10-23\`!`
    );

    callTimeStampChannel.send(
      `**Stream Title:** ${lastStreamTitle}\n**Date of Stream:** ${moment()
        .tz("America/Los_Angeles")
        .format(
          "M/D/YYYY"
        )}\n**Start of Call Timestamp:** ${hours}:${minutes}:${seconds}\n**Brief Description of Call:** ${args.join(
        " "
      )}`
    );
  }
}
